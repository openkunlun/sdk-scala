/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import com.google.protobuf.ByteString
import io.openkunlun.scaladsl.serialization.{ DaprObjectSerialization, JsonSerialization }
import io.openkunlun.scaladsl.v1.{ InvokeRequest, InvokeResponse, InvokeService }

import scala.concurrent.{ ExecutionContext, Future }

/**
 * @author ericxin.
 */
private object DaprInvocationClient extends ExtensionId[DaprInvocationClient] with ExtensionIdProvider {
  override def lookup: ExtensionId[DaprInvocationClient] = DaprInvocationClient
  override def createExtension(system: ExtendedActorSystem) = new DaprInvocationClient(system)
}
private class DaprInvocationClient(system: ExtendedActorSystem) extends Extension {

  private val serialization: DaprObjectSerialization = JsonSerialization(system)
  private val clientContext = DaprGrpcClient(system)
  private val client = clientContext.client

  /**
   *
   * @param in
   * @return
   */
  def invokeService(in: io.openkunlun.scaladsl.v1.InvokeService, headers: Map[String, String] = Map.empty): scala.concurrent.Future[io.openkunlun.scaladsl.v1.InvokeResponse] = {
    if (headers.nonEmpty) {
      var builder = client.invokeService()
      if (headers.nonEmpty) {
        headers.foreach(it => builder = builder.addHeader(it._1, it._2))
      }
      builder.invoke(in)
    } else client.invokeService(in)
  }

  /**
   *
   * @param in
   * @return
   */
  def invokeAtService(serviceHost: String, servicePort: Int, in: io.openkunlun.scaladsl.v1.InvokeService, headers: Map[String, String] = Map.empty): scala.concurrent.Future[io.openkunlun.scaladsl.v1.InvokeResponse] = {
    val c = clientContext.clientOf(serviceHost, servicePort)
    if (headers.nonEmpty) {
      var builder = c.invokeService()
      if (headers.nonEmpty) {
        headers.foreach(it => builder = builder.addHeader(it._1, it._2))
      }
      builder.invoke(in)
    } else c.invokeService(in)
  }

  /**
   *
   * @param action
   * @param headers
   * @param ec
   * @param serialization
   * @tparam T
   * @return
   */
  def invokeMethod[T: Manifest](action: InvokeMethodAction, headers: Map[String, String] = Map.empty)(implicit ec: ExecutionContext, serialization: DaprObjectSerialization = serialization): Future[T] = {

    var builder = client.invokeService()
    if (headers.nonEmpty) {
      headers.foreach(it => builder = builder.addHeader(it._1, it._2))
    }

    try {
      val data = serializeData(action.data)
      for {
        response <- builder.invoke(InvokeService(action.id, Some(InvokeRequest(action.method, data, action.contentType.getOrElse("")))))
        result = deserializeData[T](response)
      } yield result.getOrElse(throw new DaprInvocationResultIsEmptyException(action.method))
    } catch {
      case e: Throwable => Future.failed(e)
    }
  }

  private def serializeData(data: AnyRef)(implicit serialization: DaprObjectSerialization): Option[com.google.protobuf.any.Any] = {
    if (data != null) {
      Some(com.google.protobuf.any.Any(value = ByteString.copyFrom(serialization.serialize(data))))
    } else Option.empty[com.google.protobuf.any.Any]
  }

  private def deserializeData[T: Manifest](response: InvokeResponse)(implicit serialization: DaprObjectSerialization): Option[T] = {
    response.data.map(_.value.toByteArray).map(it => serialization.deserialize[T](it))
  }
}
