/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import akka.grpc.GrpcClientSettings
import io.openkunlun.scaladsl.v1

import java.util.concurrent.locks.ReentrantReadWriteLock
import scala.concurrent.duration.DurationInt

/**
 * @author ericxin.
 */
private[scaladsl] object DaprGrpcClient extends ExtensionId[DaprGrpcClient] with ExtensionIdProvider {
  override def lookup: ExtensionId[DaprGrpcClient] = DaprGrpcClient
  override def createExtension(system: ExtendedActorSystem) = new DaprGrpcClient(system)
}
private[scaladsl] class DaprGrpcClient(system: ExtendedActorSystem) extends Extension {

  private val lock = new ReentrantReadWriteLock()

  private val maxInboundMessageSize: Int = system.settings.config.getConfig("akka.grpc.client.\"dapr\"").getInt("max-inbound-message-size")
  private val maxInboundMetadataSize: Int = system.settings.config.getConfig("akka.grpc.client.\"dapr\"").getInt("max-inbound-metadata-size")

  val clientSettings: GrpcClientSettings = GrpcClientSettings.fromConfig("dapr")(system)
    .withChannelBuilderOverrides(_.maxInboundMetadataSize(maxInboundMetadataSize).maxInboundMessageSize(maxInboundMessageSize))
    .withUserAgent("sdk-scala/v1")
  val clientHost: String = clientSettings.serviceName
  val clientPort: Int = clientSettings.defaultPort
  val client: v1.DaprClient = io.openkunlun.scaladsl.v1.DaprClient(clientSettings.withTls(clientSettings.trustManager.nonEmpty))(system)

  private var clients: Map[String, v1.DaprClient] = Map.empty
  clients = clients + (s"$clientHost:$clientPort" -> client)

  def clientOf(host: String, port: Int): v1.DaprClient = {
    val key = s"$host:$port"
    readOps(clients.get(key)) match {
      case Some(c) => c
      case None =>
        writeOps {
          val settings: GrpcClientSettings = GrpcClientSettings.connectToServiceAt(host, port)(system)
            .withChannelBuilderOverrides(_.maxInboundMetadataSize(maxInboundMetadataSize).maxInboundMessageSize(maxInboundMessageSize))
            .withUserAgent("sdk-scala/v1")

          val c = io.openkunlun.scaladsl.v1.DaprClient(settings.withDeadline(clientSettings.deadline).withTls(clientSettings.trustManager.nonEmpty))(system)
          clients = clients + (key -> c)
          c
        }
    }
  }

  private def readOps[T](block: => T): T = {
    try {
      lock.readLock().lock()
      block
    } finally {
      lock.readLock().unlock()
    }
  }

  private def writeOps[T](block: => T): T = {
    try {
      lock.writeLock().lock()
      block
    } finally {
      lock.writeLock().unlock()
    }
  }
}
