/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import io.openkunlun.scaladsl.serialization.{ DaprObjectSerialization, JsonSerialization }

/**
 * @author ericxin.
 */
private object DaprMetadataClient extends ExtensionId[DaprMetadataClient] with ExtensionIdProvider {
  override def lookup: ExtensionId[DaprMetadataClient] = DaprMetadataClient
  override def createExtension(system: ExtendedActorSystem) = new DaprMetadataClient(system)
}
private class DaprMetadataClient(system: ExtendedActorSystem) extends Extension {
  private val serialization: DaprObjectSerialization = JsonSerialization(system)
  private val daprClient = DaprGrpcClient(system)
}
