/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.util

/**
 * @author ericxin.
 */
object DaprQualifier {

  val slash = "/"

  def build(qualifier: String): String = {
    build(qualifier :: Nil)
  }

  def build(prefix: String, clazz: Class[_]): String = {
    build(prefix, clazz, None)
  }

  def build(prefix: String, clazz: Class[_], suffix: Option[String] = None): String = {
    build(Seq(prefix, clazz.getName.replaceAll("\\$", slash), suffix.getOrElse("")))
  }

  def build(qualifiers: Seq[String]): String = {
    qualifiers.filter(Strings.nonEmpty).flatMap(split).mkString(slash)
  }

  private def split(str: String): Seq[String] = str.split(slash).map(_.trim).filterNot(_.isEmpty).toList
}