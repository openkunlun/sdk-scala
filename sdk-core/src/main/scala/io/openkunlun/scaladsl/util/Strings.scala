/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.util

/**
 * @author ericxin.
 */
object Strings {

  /**
   *
   * @param str
   * @return
   */
  def isEmpty(str: String): Boolean = str == null || str.trim.isEmpty

  /**
   *
   * @param str
   * @return
   */
  def isEmpty(str: Option[String]): Boolean = isEmpty(str.orNull)

  /**
   *
   * @param str
   * @return
   */
  def nonEmpty(str: String): Boolean = !isEmpty(str)

  /**
   *
   * @param str
   * @return
   */
  def nonEmpty(str: Option[String]): Boolean = nonEmpty(str.orNull)

  /**
   *
   * @param map
   * @return
   */
  def toString(map: Map[String, String]): String = map.map(it => s"${it._1} : ${it._2}").mkString("{", ",", "}")
}
