/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

/**
 * @author ericxin.
 */
trait DaprObjectSerialization extends DaprObjectSerializer with DaprObjectDeserializer
