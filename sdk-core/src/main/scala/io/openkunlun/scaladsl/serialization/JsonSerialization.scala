/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import org.json4s._
import org.json4s.jackson.Serialization

/**
 * @author ericxin.
 */
object JsonSerialization extends ExtensionId[JsonSerialization] with ExtensionIdProvider {
  override def lookup: ExtensionId[JsonSerialization] = JsonSerialization
  override def createExtension(system: ExtendedActorSystem) = new JsonSerialization(system)
}

class JsonSerialization(system: ExtendedActorSystem) extends Extension with DaprObjectSerialization {
  private val lock = new Object()
  @volatile implicit var formats: Formats = DefaultFormats

  override def serialize[T](serializable: T): Array[Byte] = {
    Serialization.write(serializable.asInstanceOf[AnyRef])(formats).getBytes("UTF-8")
  }

  /**
   *
   * @param bytes
   * @tparam T
   * @return
   */
  override def deserialize[T: Manifest](bytes: Array[Byte]): T = {
    Serialization.read[T](new String(bytes))(formats, manifest[T])
  }

  def addFormat(append: Serializer[_]): Unit = {
    addFormats(append :: Nil)
  }

  def addFormats(appends: Seq[Serializer[_]]): Unit = {
    lock.synchronized {
      formats = formats ++ appends
    }
  }

  def addKeyFormat(append: KeySerializer[_]): Unit = {
    addKeyFormats(append :: Nil)
  }

  def addKeyFormats(appends: Seq[KeySerializer[_]]): Unit = {
    lock.synchronized {
      formats = formats.addKeySerializers(appends)
    }
  }
}