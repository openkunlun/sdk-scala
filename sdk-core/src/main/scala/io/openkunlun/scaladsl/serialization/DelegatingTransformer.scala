/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

object DelegatingTransformer {
  type Transformer = PartialFunction[String, String]
  def empty(): Transformer = PartialFunction.empty[String, String]
}
