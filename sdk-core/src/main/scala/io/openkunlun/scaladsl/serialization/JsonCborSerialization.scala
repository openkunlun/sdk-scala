/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.DeserializationFeature.USE_BIG_INTEGER_FOR_INTS
import com.fasterxml.jackson.databind.{ DeserializationFeature, SerializationFeature }
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.json4s._
import org.json4s.jackson.Json4sScalaModule

object JsonCborSerialization extends ExtensionId[JsonCborSerialization] with ExtensionIdProvider {
  override def lookup: ExtensionId[JsonCborSerialization] = JsonCborSerialization
  override def createExtension(system: ExtendedActorSystem) = new JsonCborSerialization(system)
}

class JsonCborSerialization(system: ExtendedActorSystem) extends Extension with DaprObjectSerialization {
  private val lock = new Object()
  @volatile implicit var formats: Formats = DefaultFormats

  private lazy val cborObjectMapper = {
    val m = new CBORMapper()
    m.registerModule(new Json4sScalaModule)
    m.registerModule(new DefaultScalaModule)
    m.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true)
    m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    m.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    m.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true)
    m.configure(USE_BIG_INTEGER_FOR_INTS, true)
    m
  }
  def mapper = cborObjectMapper

  override def serialize[T](serializable: T): Array[Byte] = {
    mapper.writeValueAsBytes(Extraction.decompose(serializable)(formats))
  }

  /**
   *
   * @param bytes
   * @tparam T
   * @return
   */
  override def deserialize[T: Manifest](bytes: Array[Byte]): T = {
    mapper.readValue(bytes, classOf[JValue]).extract(formats, manifest[T])
  }

  def addFormat(append: Serializer[_]): Unit = {
    addFormats(append :: Nil)
  }

  def addFormats(appends: Seq[Serializer[_]]): Unit = {
    lock.synchronized {
      formats = formats ++ appends
    }
  }

  def addKeyFormat(append: KeySerializer[_]): Unit = {
    addKeyFormats(append :: Nil)
  }

  def addKeyFormats(appends: Seq[KeySerializer[_]]): Unit = {
    lock.synchronized {
      formats = formats.addKeySerializers(appends)
    }
  }
}