/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import io.openkunlun.scaladsl.v1.SerializationManifest
import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import com.google.protobuf.ByteString
import org.json4s.jackson.Serialization
import org.json4s.{ DefaultFormats, Formats, Serializer }

import scala.reflect.ManifestFactory

/**
 * @author ericxin.
 */
object JsonDelegateSerialization extends ExtensionId[JsonDelegateSerialization] with ExtensionIdProvider {
  override def lookup: ExtensionId[JsonDelegateSerialization] = JsonDelegateSerialization
  override def createExtension(system: ExtendedActorSystem) = new JsonDelegateSerialization(system)
}

class JsonDelegateSerialization(system: ExtendedActorSystem) extends Extension with DelegatingSerialization {
  private val lock = new Object()
  implicit val formats: Formats = DefaultFormats
  var customerFormats: Seq[Serializer[_]] = Seq.empty

  override def serializeWithManifest[T](serializable: T): SerializationManifest = {
    SerializationManifest(
      serializable.getClass.getName,
      ByteString.copyFrom(Serialization.write(serializable.asInstanceOf[AnyRef])(formats ++ customerFormats).getBytes("UTF-8"))
    )
  }

  override def deserializeWithManifest[T: Manifest](format: SerializationManifest): T = {
    val clazz = system.dynamicAccess.getClassFor[T](format.manifest).get
    Serialization.read(new String(format.payload.toByteArray))(formats ++ customerFormats, ManifestFactory.classType(clazz))
  }

  def addFormat(append: Serializer[_]): Unit = {
    addFormats(append :: Nil)
  }

  def addFormats(appends: Seq[Serializer[_]]): Unit = {
    lock.synchronized {
      customerFormats = this.customerFormats ++ appends
    }
  }
}