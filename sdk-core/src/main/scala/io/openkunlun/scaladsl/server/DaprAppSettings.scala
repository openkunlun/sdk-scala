/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import com.typesafe.config.Config
import io.openkunlun.scaladsl.util.Strings

import java.net.InetAddress
import java.util.UUID
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{ DurationLong, FiniteDuration }
import scala.jdk.CollectionConverters._

/**
 * @author ericxin.
 */
private[server] class DaprAppSettings(config: Config) {

  val id: String = UUID.randomUUID().toString.replace("-", "")
  val protocol: String = config.getString("dapr.server.protocol")
  val host: String = config.getString("dapr.server.host") match {
    case "<getHostAddress>" => InetAddress.getLocalHost.getHostAddress
    case "<getHostName>"    => InetAddress.getLocalHost.getHostName
    case other              => other
  }
  val port: Int = config.getInt("dapr.server.port")
  val ttl: Int = config.getDuration("dapr.server.ttl", TimeUnit.SECONDS).toInt
  val weight: Int = config.getInt("dapr.server.weight")
  val warmup: Int = config.getInt("dapr.server.warmup")

  val roles: Seq[String] = config.getStringList("dapr.server.roles").asScala.toSeq.distinct.filter(Strings.nonEmpty)
  val bindings: Seq[String] = config.getStringList("dapr.server.bindings").asScala.toSeq.distinct.filter(Strings.nonEmpty)

  val keepAliveInterval: FiniteDuration = config.getDuration("dapr.server.keep-alive-interval", TimeUnit.SECONDS).seconds
  val keepAliveMaxInterval: FiniteDuration = config.getDuration("dapr.server.keep-alive-max-interval", TimeUnit.SECONDS).seconds
  val keepAliveRetryInterval: FiniteDuration = config.getDuration("dapr.server.keep-alive-retry-interval", TimeUnit.SECONDS).seconds
}
