/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import akka.NotUsed
import akka.actor.{ ActorSystem, Cancellable }
import akka.event.{ Logging, LoggingAdapter }
import akka.grpc.GrpcClientSettings
import akka.http.scaladsl.{ ConnectionContext, Http, HttpsConnectionContext }
import akka.pki.pem.{ DERPrivateKeyLoader, PEMDecoder }
import akka.stream.{ KillSwitch, KillSwitches, Materializer }
import akka.stream.scaladsl.{ Keep, Sink, Source }
import io.openkunlun.scaladsl.v1.{ AbolishRequest, DaprAppPowerApiHandler, DaprClient, EstablishRequest, KeepAliveRequest, KeepAliveResponse }

import java.io.InputStream
import java.security.cert.{ Certificate, X509Certificate }
import java.security.{ KeyStore, PrivateKey, SecureRandom }
import java.util.concurrent.atomic.{ AtomicBoolean, AtomicReference }
import javax.net.ssl.{ KeyManagerFactory, SSLContext, TrustManagerFactory }
import scala.concurrent.duration.{ Duration, DurationInt, FiniteDuration }
import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.util.{ Failure, Success }

/**
 * @author ericxin.
 */
class DaprServer(system: ActorSystem)(implicit ec: ExecutionContext = system.dispatcher) {

  private val app: DaprApp = new DaprApp(system)(ec)
  private val appSettings: DaprAppSettings = new DaprAppSettings(system.settings.config)
  private val appTerminated = new AtomicBoolean(false)

  private val clientSettings: GrpcClientSettings = GrpcClientSettings.fromConfig("dapr")(system)
  @volatile private var client: DaprClient = getClient

  private var keepAliveDelay: FiniteDuration = Duration.Zero
  private var keepAliveWatcher: Option[Cancellable] = None
  private val keepAliveSwitcher = new AtomicReference[KillSwitch]()
  private val keepAliveInterval: FiniteDuration = appSettings.keepAliveInterval
  private val keepAliveMaxInterval: FiniteDuration = appSettings.keepAliveMaxInterval
  private val keepAliveRetryInterval: FiniteDuration = appSettings.keepAliveRetryInterval

  private val log: LoggingAdapter = Logging(system, getClass)
  private val mat: Materializer = Materializer(system).withNamePrefix("DaprServer")
  implicit val sys: ActorSystem = system

  private var failureDetector: FailureDetector = FailureDetector.AVAILABLE

  def withFailureDetector(failureDetector: FailureDetector) = {
    this.failureDetector = failureDetector
  }

  /**
   *
   * @param handler
   */
  def addHandler(handler: DaprHandler): Unit = {
    app.addHandler(handler)
  }

  /**
   *
   * @param handlers
   */
  def addHandlers(handlers: Seq[DaprHandler]): Unit = {
    app.addHandlers(handlers)
  }

  /**
   *
   * @param handler
   */
  def addBindingHandler(handler: BindingHandler): Unit = {
    app.addBindingHandler(handler)
  }

  /**
   *
   * @param handlers
   */
  def addBindingHandlers(handlers: Seq[BindingHandler]): Unit = {
    app.addBindingHandlers(handlers)
  }

  /**
   *
   * @param handler
   */
  def addInvocationHandler(handler: InvocationHandler): Unit = {
    app.addInvocationHandler(handler)
  }

  /**
   *
   * @param handlers
   */
  def addInvocationHandlers(handlers: Seq[InvocationHandler]): Unit = {
    app.addInvocationHandlers(handlers)
  }

  /**
   *
   * @return
   */
  def start(): Future[Http.ServerBinding] = {
    start(appSettings.host, appSettings.port)
  }

  /**
   *
   * @param host
   * @param port
   * @return
   */
  def start(host: String, port: Int): Future[Http.ServerBinding] = {
    run(host, port, None)
  }

  /**
   *
   * @param pkcs12
   * @return
   */
  def start(pkcs12: InputStream): Future[Http.ServerBinding] = {
    start(appSettings.host, appSettings.port, pkcs12)
  }

  /**
   *
   * @param host
   * @param port
   * @param pkcs12
   * @return
   */
  def start(host: String, port: Int, pkcs12: InputStream): Future[Http.ServerBinding] = {
    val httpCtx = buildHttpsConnectionContext(pkcs12, None)
    run(host, port, Some(httpCtx))
  }

  def start(pkcs12: InputStream, password: String): Future[Http.ServerBinding] = {
    start(appSettings.host, appSettings.port, pkcs12, password)
  }

  def start(host: String, port: Int, pkcs12: InputStream, password: String): Future[Http.ServerBinding] = {
    val httpCtx = buildHttpsConnectionContext(pkcs12, Some(password))
    run(host, port, Some(httpCtx))
  }

  def start(privateKey: String, keyCertChain: X509Certificate): Future[Http.ServerBinding] = {
    start(appSettings.host, appSettings.port, privateKey, keyCertChain)
  }

  def start(host: String, port: Int, privateKey: String, keyCertChain: X509Certificate): Future[Http.ServerBinding] = {
    val httpCtx = buildHttpsConnectionContext(privateKey, keyCertChain, None)
    run(host, port, Some(httpCtx))
  }

  def start(privateKey: String, keyPassword: String, keyCertChain: X509Certificate): Future[Http.ServerBinding] = {
    start(appSettings.host, appSettings.port, privateKey, keyPassword, keyCertChain)
  }

  def start(host: String, port: Int, privateKey: String, keyPassword: String, keyCertChain: X509Certificate): Future[Http.ServerBinding] = {
    val httpCtx = buildHttpsConnectionContext(privateKey, keyCertChain, Some(keyPassword))
    run(host, port, Some(httpCtx))
  }

  private def run(host: String, port: Int, httpCtx: Option[HttpsConnectionContext]): Future[Http.ServerBinding] = {
    val serverBuilder = Http(system).newServerAt(interface = host, port = port).logTo(log)
    val serverBinding: Future[Http.ServerBinding] = if (httpCtx.nonEmpty) {
      serverBuilder
        .enableHttps(httpCtx.get)
        .bind(DaprAppPowerApiHandler(app))
        .map(_.addToCoordinatedShutdown(hardTerminationDeadline = 10.seconds))
    } else {
      serverBuilder
        .bind(DaprAppPowerApiHandler(app))
        .map(_.addToCoordinatedShutdown(hardTerminationDeadline = 10.seconds))
    }

    (for {
      result <- serverBinding
      _ <- runApp
    } yield result).onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        log.warning("DaprApp up. listen on {}:{}.", address.getHostString, address.getPort)
      case Failure(ex) =>
        log.warning("DaprApp die.", ex)
        Await.result(system.terminate(), Duration.Inf)
    }
    serverBinding
  }

  private def runApp: Future[Unit] = {
    if (appSettings.roles.nonEmpty || appSettings.bindings.nonEmpty) {
      for {
        _ <- startApp(reconnect = false)
        _ = system.registerOnTermination(stopApp())
      } yield ()
    } else Future.unit
  }

  private def startApp(reconnect: Boolean): Future[_] = {
    if (appSettings.roles.nonEmpty || appSettings.bindings.nonEmpty) {
      (for {
        _ <- if (reconnect) detectApp() else Future.unit
        response <- client.establishService(
          EstablishRequest(
            appSettings.id,
            appSettings.host,
            appSettings.port,
            if ("http".equalsIgnoreCase(appSettings.protocol)) EstablishRequest.Protocol.HTTP else EstablishRequest.Protocol.GRPC,
            appSettings.roles,
            appSettings.bindings,
            appSettings.weight,
            appSettings.warmup,
            System.currentTimeMillis(),
            appSettings.ttl
          )
        )
        _ = keepAliveDelay = Duration.Zero
        _ = keepAliveApp()
        _ = log.warning("DaprApp started. [id: {}, ttl: {}, roles: {}, bindings: {}].", response.id, response.ttl, appSettings.roles.mkString("[", ",", "]"), appSettings.bindings.mkString("[", ",", "]"))
      } yield ()) recoverWith {
        case e =>
          log.error("DaprApp start failed. cause: [{}].", Option(e.getCause).map(_.toString).getOrElse(e.getMessage))
          restartApp()
          Future.unit
      }
    } else Future.unit
  }

  private def stopApp(): Future[Unit] = {
    appTerminated.set(true)
    for {
      _ <- client.abolishService(AbolishRequest(appSettings.id))
      _ = log.warning("DaprApp abolished, [id: {}, roles: {}].", appSettings.id, appSettings.roles.mkString("[", ",", "]"))
      _ = Option(keepAliveSwitcher.get()).foreach(_.shutdown())
      _ <- client.close()
      _ = keepAliveWatcher.foreach(_.cancel())
      _ = log.warning("DaprApp stopped.")
    } yield ()
  }

  private def restartApp(): Unit = {
    if (!appTerminated.get()) {
      log.warning("DaprApp restarting ...")
      client.close()
      client = getClient
      keepAliveWatcher.foreach(_.cancel())
      keepAliveWatcher = Some(system.scheduler.scheduleOnce(keepAliveDelay)(startApp(reconnect = true)))
      keepAliveDelay = if (keepAliveDelay.plus(keepAliveRetryInterval) > keepAliveMaxInterval) keepAliveMaxInterval else keepAliveDelay.plus(keepAliveMaxInterval)
    }
  }

  private def keepAliveApp(): Unit = {
    val requestStream: Source[KeepAliveRequest, NotUsed] = Source.tick(keepAliveInterval, keepAliveInterval, KeepAliveRequest(appSettings.id))
      .mapMaterializedValue(_ => NotUsed)

    val responseStream: Source[KeepAliveResponse, NotUsed] = client.keepAliveService(requestStream)
    val (killSwitch, response) = responseStream.viaMat(KillSwitches.single)(Keep.right).mapAsync(1)(it => detectApp().map(_ => it)).toMat(Sink.foreach(it => log.info("Keep alive DaprApp. [id: {}, ttl: {}]", it.id, it.ttl)))(Keep.both).run()(mat)
    val eldKillSwitch = keepAliveSwitcher.getAndSet(killSwitch)
    Option(eldKillSwitch).foreach(_.shutdown())
    response onComplete {
      case Success(_) =>
        log.warning("Keep alive interrupted, restart DaprApp.")
        restartApp()
      case Failure(e) =>
        log.error("Keep alive failed. cause: [{}]. restart DaprApp.", Option(e.getCause).map(_.toString).getOrElse(e.getMessage))
        restartApp()
    }
  }

  private def detectApp(): Future[Unit] = {
    (for {
      _ <- failureDetector.detect()
      _ = log.info("DaprApp {} available.", appSettings.id)
    } yield ()) recoverWith {
      case e =>
        log.error(e, "DaprApp {} unavailable, cause: {}", appSettings.id, e.getMessage)
        Future.failed(e)
    }
  }

  private def getClient: DaprClient = {
    DaprClient(clientSettings.withDeadline(Duration.Inf).withTls(clientSettings.trustManager.nonEmpty))(system)
  }

  private val PKCS12: String = "PKCS12"
  private val TLS: String = "TLS"
  private val SunX509: String = "SunX509"
  private val emptyPassword: Array[Char] = new Array[Char](0)

  private def buildHttpsConnectionContext(privateKey: String, keyCertChain: X509Certificate, keyPassword: Option[String]): HttpsConnectionContext = {
    val pk: PrivateKey = DERPrivateKeyLoader.load(PEMDecoder.decode(privateKey))
    val ks = KeyStore.getInstance(PKCS12)
    ks.load(null)
    ks.setKeyEntry(
      "private",
      pk,
      keyPassword.map(_.toCharArray).getOrElse(emptyPassword),
      Array[Certificate](keyCertChain)
    )
    val keyManagerFactory = KeyManagerFactory.getInstance(SunX509)
    keyManagerFactory.init(ks, null)
    val context = SSLContext.getInstance(TLS)
    context.init(keyManagerFactory.getKeyManagers, null, new SecureRandom)
    ConnectionContext.httpsServer(context)
  }

  private def buildHttpsConnectionContext(pkcs12: InputStream, password: Option[String]): HttpsConnectionContext = {
    val passwordCharArray = password.map(_.toCharArray).getOrElse(emptyPassword)
    val ks: KeyStore = KeyStore.getInstance(PKCS12)
    ks.load(pkcs12, passwordCharArray)
    val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance(SunX509)
    keyManagerFactory.init(ks, passwordCharArray)

    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(SunX509)
    tmf.init(ks)

    val sslContext: SSLContext = SSLContext.getInstance(TLS)
    sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom)
    ConnectionContext.httpsServer(sslContext)
  }
}