/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

/**
 * @author ericxin.
 */
class BindingHandlerNotFoundException(bindingName: String) extends RuntimeException {
  override def getMessage: String = s"Dapr binding handler for $bindingName not found."
}
