/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import scala.concurrent.Future

/**
 * @author ericxin.
 */
trait FailureDetector {
  def detect(): Future[Unit]
}

object FailureDetector {
  val AVAILABLE: FailureDetector = () => Future.unit
  val UNAVAILABLE: FailureDetector = () => Future.failed(new RuntimeException("Service unavailable."))
}