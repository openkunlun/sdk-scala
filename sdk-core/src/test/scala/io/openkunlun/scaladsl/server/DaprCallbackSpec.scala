/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import akka.grpc.scaladsl.Metadata
import akka.util.Timeout
import io.openkunlun.scaladsl.serialization.DaprObjectSerialization
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.{ SerializationDto1, SerializationDto2 }
import io.openkunlun.scaladsl.server.DaprServerBootstrapSpec.ser

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ ExecutionContext, Future }

/**
 * @author ericxin.
 */
object DaprCallbackSpec extends App {

  def mutable() = {
    val map = scala.collection.mutable.Map.empty[String, InvocationHandler]

    val t0 = System.currentTimeMillis()
    for (i <- 1 to 10000000) {
      val uuid = java.util.UUID.randomUUID().toString
      val handler = new InvocationService[SerializationDto1, SerializationDto2] {
        override val serialization: DaprObjectSerialization = ser
        implicit val deadline: Timeout = 30.seconds

        override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
          println("AbstractDaprInvocationHandler: " + req.data)
          Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
        }

        override val methods: Seq[String] = Seq(uuid + i)
      }
      handler.methods.foreach(method => map.put(method, handler))
    }

    val uuid = java.util.UUID.randomUUID().toString
    val handler = new InvocationService[SerializationDto1, SerializationDto2] {
      override val serialization: DaprObjectSerialization = ser
      implicit val deadline: Timeout = 30.seconds

      override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
        println("AbstractDaprInvocationHandler: " + req.data)
        Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
      }

      override val methods: Seq[String] = Seq(uuid)
    }
    handler.methods.foreach(method => map.put(method, handler))
    println("mutable: add " + map.size + " handlers user time:" + (System.currentTimeMillis() - t0))
    println(map.contains(uuid))
  }

  def inMutable() = {
    var map = Map.empty[String, InvocationHandler]

    val t0 = System.currentTimeMillis()
    for (i <- 1 to 10000000) {
      val uuid = java.util.UUID.randomUUID().toString
      val handler = new InvocationService[SerializationDto1, SerializationDto2] {
        override val serialization: DaprObjectSerialization = ser
        implicit val deadline: Timeout = 30.seconds

        override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
          println("AbstractDaprInvocationHandler: " + req.data)
          Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
        }

        override val methods: Seq[String] = Seq(uuid + i)
      }
      handler.methods.foreach(method => map = map + (method -> handler))
    }

    val uuid = java.util.UUID.randomUUID().toString
    val handler = new InvocationService[SerializationDto1, SerializationDto2] {
      override val serialization: DaprObjectSerialization = ser
      implicit val deadline: Timeout = 30.seconds

      override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
        println("AbstractDaprInvocationHandler: " + req.data)
        Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
      }

      override val methods: Seq[String] = Seq(uuid)
    }
    handler.methods.foreach(method => map = map + (method -> handler))
    println("inMutable: add " + map.size + " handlers user time:" + (System.currentTimeMillis() - t0))
    println(map.contains(uuid))
  }
  inMutable()
  mutable()
}
