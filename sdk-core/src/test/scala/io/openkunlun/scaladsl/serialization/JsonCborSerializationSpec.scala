/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.ActorSystem
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import io.openkunlun.scaladsl.util.DaprQualifierSpec.Bar
import org.json4s.{CustomKeySerializer, DefaultFormats}
import org.json4s.ext.EnumSerializer
import org.json4s.jackson.Serialization

/**
 * @author ericxin.
 */
object JsonCborSerializationSpec extends App {
  val sys = ActorSystem("JsonSerializationSpec")

  final case class Foo(a: String, b: Int, @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS) c: Bar.Type, biz: Biz, map: Map[Key, String])
  final case class Biz(a: String, b: Int, c: Bar.Type)
  final case class Key(a: String)

  object Bar extends Enumeration {
    type Type = Value
    val PersonalType: String = "personal"
    val Personal = Value(PersonalType)
    val EnterpriseType: String = "enterprise"
    val Enterprise = Value(EnterpriseType)
  }
  object Bar1 extends Enumeration {
    type Type = Value
    val PersonalType: String = "personal"
    val Personal = Value(PersonalType)
    val EnterpriseType: String = "enterprise"
    val Enterprise = Value(EnterpriseType)
  }

  val BarMap = Map(
    Bar.Personal -> "Bar.Personal",
    Bar.Enterprise -> "Bar.Enterprise"
  )

  val foo = Foo("A", 1, Bar.Personal, Biz("b", 2, Bar.Enterprise), Map(Key("key") -> "aa"))
  implicit val formats = DefaultFormats
  val ser = JsonCborSerialization(sys)

  ser.addFormat(new EnumSerializer(Bar))
  ser.addFormat(new EnumSerializer(Bar1))

  case object ParticipantKeySerializer extends CustomKeySerializer[Key](
    _ => ({
      case s => Serialization.read[Key](s)
    }, {
      case it: Key => Serialization.write(it)
    })
  )

  ser.addKeyFormat(ParticipantKeySerializer)

  //  val t0 = System.currentTimeMillis()
  //  for( i <- 1 to 100000){
  //    val bytes = ser.serialize(Foo("foo", 1, true))
  //    val result = ser.deserialize[Foo](bytes)
  //  }
  //  println(System.currentTimeMillis() - t0)

  val bytes = ser.serialize(foo)
  println(new String(bytes))
  println(BarMap(foo.c))
  println(" ------ ")
  val result = ser.deserialize[Foo](bytes)
  println(result)
  println(BarMap(result.c))

//  implicit val formats = DefaultFormats
//  println(Serialization.read[Map[String, Any]](new String(bytes)))
}
