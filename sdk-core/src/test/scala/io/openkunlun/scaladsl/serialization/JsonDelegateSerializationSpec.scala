/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.ActorSystem

/**
 * @author ericxin.
 */
object JsonDelegateSerializationSpec extends App {
  val sys = ActorSystem("JsonSerializationSpec")
  case class Foo(a: String, b: Int, c: Boolean)

  val ser = JsonDelegateSerialization(sys)

  //  val t0 = System.currentTimeMillis()
  //  for( i <- 1 to 100000){
  //    val bytes = ser.serialize(Foo("foo", 1, true))
  //    val result = ser.deserialize[Foo](bytes)
  //  }
  //  println(System.currentTimeMillis() - t0)

  val bytes = ser.serialize(Foo("foo", 1, true))
  val result = ser.deserialize[Foo](bytes)
  println(new String(bytes))
  println(result)
}
