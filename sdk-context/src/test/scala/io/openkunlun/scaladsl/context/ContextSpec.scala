/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

import akka.actor.ActorSystem
import kamon.Kamon
import kamon.tag.Tag

import scala.concurrent.Future

/**
 * @author ericxin.
 */
object ContextSpec extends App {

  val system = ActorSystem("ac")

  import system.dispatcher

  var kamonConfig = Kamon.config()
  if (system.settings.config.getBoolean("dapr.context.instrumentation.akka-http")) {
    kamonConfig = kamonConfig.withFallback(system.settings.config.getConfig(s"dapr.context.${KamonContextStorage.Name}.akka-http"))
  }
  if (system.settings.config.getBoolean("dapr.context.instrumentation.play")) {
    kamonConfig = kamonConfig.withFallback(system.settings.config.getConfig(s"dapr.context.${KamonContextStorage.Name}.play"))
  }

  println(Kamon.config().getConfig("kamon.propagation.http.default"))
  println(kamonConfig.getConfig("kamon.propagation.http.default"))
  println(" ------------------- ")
  println(Kamon.config().getConfig("kamon.instrumentation.akka.http"))
  println(kamonConfig.getConfig("kamon.instrumentation.akka.http"))
  println(" ------------------- ")
  println(Kamon.config().getConfig("kamon.instrumentation.play.http"))
  println(kamonConfig.getConfig("kamon.instrumentation.play.http"))
  println(" ------------------- ")

  val context = ContextStorageKeeper(system).current

  println(context)
  context.init()

  context.store(Context.of("foo", "bar")) {
    Future {
      // is available here as well.
      "Hello Kamon"

    }.map(_.length)
      .flatMap(s => Future.successful(s + ":" + s.toString))
      .map(s => s"$s : ${context.get().get("foo").getOrElse("nop")}")
      .flatMap(s => Future.successful(s + "!!!!"))
      .map(println)
    // And through all async callbacks, even though
    // they are executed in different threads!
  }
}
