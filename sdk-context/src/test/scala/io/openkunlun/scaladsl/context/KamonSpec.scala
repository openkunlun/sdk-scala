/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

import akka.actor.ActorSystem
import kamon.Kamon

import scala.concurrent.Future

/**
 * @author ericxin.
 */
object KamonSpec extends App {

  val system = ActorSystem("ac")

  import system.dispatcher

  Kamon.init()

  Kamon.runWithContext(kamon.context.Context.of("foo", "bar")){
    for {
      _ <- Future.successful("biz")
      _ = println(s"In future context hashcode: ${Kamon.currentContext().hashCode()}, tags: ${Kamon.currentContext().tags}")
    } yield {
      println(s"In yield context hashcode: ${Kamon.currentContext().hashCode()}, tags: ${Kamon.currentContext().tags}")
    }
  }
}
