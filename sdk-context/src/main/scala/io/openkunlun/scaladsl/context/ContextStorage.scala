/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }

/**
 * @author ericxin.
 */
trait ContextStorage {
  def init(): Unit
  def store[T](ctx: Context)(f: => T): T
  def get(): Context
}

private[openkunlun] object ContextStorageKeeper extends ExtensionId[ContextStorageKeeper] with ExtensionIdProvider {
  override def lookup: ExtensionId[ContextStorageKeeper] = ContextStorageKeeper
  override def createExtension(system: ExtendedActorSystem) = new ContextStorageKeeper(system)
}

private[openkunlun] class ContextStorageKeeper(system: ExtendedActorSystem) extends Extension {

  private val Nop = new NopContextStorage()

  val current: ContextStorage = {
    if (system.settings.config.getBoolean("dapr.context.enabled")) {
      system.settings.config.getString("dapr.context.provider") match {
        case "kamon" => KamonContextStorage(system)
        case _       => Nop
      }
    } else Nop
  }
}