### 1.0.2
#### Dependency
~~~
  AkkaVersion = "2.6.19"
  AkkaHttpVersion = "10.2.9"
  AkkaGrpcVersion = "2.1.4"
  GrpcVersion = "1.45.0"
  Json4sVersion = "3.6.12"
  KamonVersion = "2.5.0"
  PlayVersion = "2.8.13"
~~~