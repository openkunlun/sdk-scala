/*
 * Copyright 2014-2020 Forever High Technology <http://www.foreverht.com>
 */

import sbt._

object ProjectDependencyVersions {
  val AkkaVersion = "2.6.21"
  val AkkaHttpVersion = "10.2.10"
  val AkkaGrpcVersion = "2.1.6"
  val PlayVersion = "2.8.22"

  val GrpcVersion = "1.59.1"
  val Json4sVersion = "3.6.12"
  val KamonVersion = "2.7.3"
}

object ProjectDependencies {
  import ProjectDependencyVersions._

  val Javaslang =                 "io.javaslang"                           % "javaslang"                        % "2.0.6"
  val JunitInterface =            "com.novocode"                           % "junit-interface"                  % "0.11"

  val AkkaActor =                 "com.typesafe.akka"                     %% "akka-actor"                       % AkkaVersion exclude("org.scala-lang.modules", "scala-java8-compat")
  val AkkaStream =                "com.typesafe.akka"                     %% "akka-stream"                      % AkkaVersion
  val AkkaDiscovery =             "com.typesafe.akka"                     %% "akka-discovery"                   % AkkaVersion
  val AkkaPki =                   "com.typesafe.akka"                     %% "akka-pki"                         % AkkaVersion
  val AkkaActorTyped =            "com.typesafe.akka"                     %% "akka-actor-typed"                 % AkkaVersion
  val AkkaSlf4j =                 "com.typesafe.akka"                     %% "akka-slf4j"                       % AkkaVersion
  val AkkaSerializationJackson =  "com.typesafe.akka"                     %% "akka-serialization-jackson"       % AkkaVersion

  val AkkaGrpcRuntime =           "com.lightbend.akka.grpc"               %% "akka-grpc-runtime"                % AkkaGrpcVersion

  val AkkaHttp =                  "com.typesafe.akka"                     %% "akka-http"                        % AkkaHttpVersion exclude("com.typesafe.akka", "akka-stream")
  val AkkaHttp2Support =          "com.typesafe.akka"                     %% "akka-http2-support"               % AkkaHttpVersion exclude("com.typesafe.akka", "akka-stream")

  val GrpcProtobuf =              "io.grpc"                                % "grpc-protobuf"                    % GrpcVersion
  val GrpcCore =                  "io.grpc"                                % "grpc-core"                        % GrpcVersion
  val GrpcStub =                  "io.grpc"                                % "grpc-stub"                        % GrpcVersion
  val GrpcNettyShaded =           "io.grpc"                                % "grpc-netty-shaded"                % GrpcVersion

  val Json4sJackson =             "org.json4s"                            %% "json4s-jackson"                   % Json4sVersion exclude("com.fasterxml.jackson.core", "jackson-databind")
  val Json4sExt =                 "org.json4s"                            %% "json4s-ext"                       % Json4sVersion

  val Play =                      "com.typesafe.play"                     %% "play"                             % PlayVersion

  val JacksonDatabind =           "com.fasterxml.jackson.core"             % "jackson-databind"                 % "2.11.4"
  val JacksonDataFormatCbor =     "com.fasterxml.jackson.dataformat"       % "jackson-dataformat-cbor"          % "2.11.4"
  val jacksonModuleScala =        "com.fasterxml.jackson.module"           % "jackson-module-scala_2.13"        % "2.11.4"
  val ScalapbRuntime =            "com.thesamet.scalapb"                  %% "scalapb-runtime"                  % "0.11.13"

  val KamonCore =                 "io.kamon"                              %% "kamon-core"                       % KamonVersion
  val KamonScalaFuture =          "io.kamon"                              %% "kamon-scala-future"               % KamonVersion
  val KamonAkka =                 "io.kamon"                              %% "kamon-akka"                       % KamonVersion
  val KamonAkkaHttp =             "io.kamon"                              %% "kamon-akka-http"                  % KamonVersion
  val KamonPlay =                 "io.kamon"                              %% "kamon-play"                       % KamonVersion

  val Scalameta =                 "org.scalameta"                         %% "scalameta"                        % "4.5.13"

  val Scalatest =                 "org.scalatest"                         %% "scalatest"                        % "3.1.2"
}