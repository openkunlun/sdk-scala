/*
 * Copyright 2015 - 2020 Forever High Tech <http://www.foreverht.com> - all rights reserved.
 */


import com.typesafe.sbt.SbtScalariform.autoImport._
import de.heikoseeberger.sbtheader.HeaderPlugin.autoImport._
import de.heikoseeberger.sbtheader.License
import sbt.Keys._
import sbt._
import scalariform.formatter.preferences._

object ProjectSettings {

  lazy val rootSettings: Seq[Setting[_]] =
    compilerSettings ++
      headerSettings ++
      testSettings ++
      publishSettings

  lazy val rdcCredentials: Seq[Credentials] = Seq(Credentials(Path.userHome / ".sbt" / ".snapshotCredentials"), Credentials(Path.userHome / ".sbt" / ".releaseCredentials"))
  lazy val rdcReleases: MavenRepository = "rdc-releases" at "https://packages.aliyun.com/maven/repository/2125526-release-qv8CDO/"
  lazy val rdcSnapshot: MavenRepository = "rdc-snapshots" at "https://packages.aliyun.com/maven/repository/2125526-snapshot-KXVRcj/"

  lazy val commonSettings: Seq[Setting[_]] =
    rootSettings ++ formatterSettings ++ Seq(
      resolvers += Resolver.mavenLocal,
      credentials ++= rdcCredentials,
      resolvers += rdcReleases,
      resolvers += rdcSnapshot
    )

  lazy val publishSettings: Seq[Setting[_]] = {
    val releaseRepo = Some(rdcReleases)
    val snapshotRepo = Some(rdcSnapshot)

    Seq(
      credentials ++= rdcCredentials,
      publishTo := (if (isSnapshot.value) snapshotRepo else releaseRepo),
      publishMavenStyle := true
    )
  }

  // ----------------------------------------------------------------------
  //  Common settings
  // ----------------------------------------------------------------------

  lazy val compilerSettings: Seq[Setting[_]] = Seq(
    scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xlint"),
    Compile / doc / sources := Seq.empty,
    Compile / packageDoc / publishArtifact := false
  )

  lazy val testSettings: Seq[Setting[_]] = Seq(
    Test / parallelExecution := false,
    Test / fork := true
  )

  lazy val headerSettings: Seq[Setting[_]] = {
    Seq(
      headerLicense := Some(License.Custom(
        """Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>""".stripMargin
      )),
      headerMappings := headerMappings.value + (HeaderFileType.scala -> HeaderCommentStyle.cStyleBlockComment) + (HeaderFileType.java -> HeaderCommentStyle.cStyleBlockComment)
    )
  }


  lazy val integrationTestPublishSettings = Seq(
    Test / publishArtifact := true
  ) ++ Classpaths.defaultPackageKeys.flatMap(tk => addArtifact(IntegrationTest / tk / artifact, IntegrationTest / tk))

  // ----------------------------------------------------------------------
  //  Code formatter settings
  // ----------------------------------------------------------------------

  lazy val formatterSettings: Seq[Setting[_]] = {
    Seq(
      scalariformPreferences := scalariformPreferences.value
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(DoubleIndentConstructorArguments, false)
        .setPreference(DanglingCloseParenthesis, Preserve)
    )
  }
}