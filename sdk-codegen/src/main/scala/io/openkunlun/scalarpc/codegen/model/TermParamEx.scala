/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta._

/**
 * @author kostas.kougios
 *         Date: 29/09/17
 */
case class TermParamEx private (meta: TermParamEx.Meta) extends CodeEx
  with MetaEx.Contains
  with MetaEx.ContainsMods[TermParamEx]
  with CodeEx.Name[TermParamEx] {

  override def tree: Term.Param = meta.param
  override def name: String = meta.param.name.value
  override def withName(name: String): TermParamEx = copy(meta = meta.copy(param = meta.param.copy(name = Name(name))))
  override def withMods(mods: ModsEx): TermParamEx = copy(meta = meta.copy(param = meta.param.copy(mods = mods.meta.mods.toList)))

  def `type`: Option[TypeEx] = meta.param.decltpe.map(TypeEx.apply)
  def withType(tpe: String): TermParamEx = copy(meta = meta.copy(param = meta.param.copy(decltpe = Some(tpe.parse[Type].get))))

  def toVal: ValEx = {
    val p = meta.param
    //    val q = q"..${p.mods} val ..${List(Pat.Var(Term.Name(p.name.value)))}: ${p.decltpe.get}"
    //    ValEx.parser(q)
    ???
  }

}
object TermParamEx {

  case class Meta(param: scala.meta.Term.Param) extends MetaEx with MetaEx.Mods {
    override def mods = param.mods
  }

  def apply(param: scala.meta.Term.Param): TermParamEx = TermParamEx(Meta(param))
  def fromSource(code: String): TermParamEx = TermParamEx(Meta(code.parse[Term.Param].get))
}