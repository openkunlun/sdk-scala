/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta.Tree

/**
 * @author kostas.kougios
 *         Date: 08/09/17
 */
trait CodeEx {
  def tree: Tree
  def syntax: String = tree.syntax
  def structure: String = tree.structure

  override def equals(o: Any): Boolean = o match {
    case c: CodeEx => c.syntax.trim == syntax.trim
    case _         => super.equals(o)
  }

  override def hashCode: Int = syntax.hashCode
  override def toString: String = syntax
}

object CodeEx {

  trait Name[+T] {
    def name: String
    def withName(name: String): T
    //		def unCapitalizedName: String = WordUtils.uncapitalize(name)
  }

}