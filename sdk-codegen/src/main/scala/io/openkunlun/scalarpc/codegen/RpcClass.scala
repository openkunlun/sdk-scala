/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen

case class RpcClass(
  packageImports: Seq[String],
  packageName: String,
  className: String,
  appName: String,
  methods: Seq[RpcMethod]
)
case class RpcMethod(
  methodId: String,
  methodName: String,
  parameters: Seq[RpcParameter],
  returnType: String,
  returnTypeArgs: Seq[String],
  implemented: Boolean,
  ignored: Boolean = false
)
case class RpcParameter(name: String, tpe: String)
