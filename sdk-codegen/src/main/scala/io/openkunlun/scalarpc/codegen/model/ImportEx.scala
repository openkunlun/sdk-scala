/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta._

/**
 * @author kostas.kougios
 *         Date: 02/11/17
 */
case class ImportEx private (meta: ImportEx.Meta) extends CodeEx {
  override def tree: Import = Import(meta.importersnel.toList)
  def imports: Seq[Imported] = meta.importersnel.flatMap { i =>
    i.importees.collect {
      case ie: Importee.Name =>
        NameImport(i.ref.toString, ie.name.value)
      case ie: Importee.Rename =>
        RenameImport(i.ref.toString, ie.name.value, ie.rename.value)
      case _: Importee.Wildcard =>
        WildcardImport(i.ref.toString)
    }
  }
  def nameImports: Seq[NameImport] = imports.collect {
    case t @ NameImport(_, _) => t
  }

  def renameImports: Seq[RenameImport] = imports.collect {
    case t @ RenameImport(_, _, _) => t
  }

  def wildcardImports: Seq[WildcardImport] = imports.collect {
    case t @ WildcardImport(_) => t
  }
}
object ImportEx extends PartialParser[ImportEx] {
  case class Meta(importersnel: Seq[Importer]) extends MetaEx

  override def parser: PartialFunction[Tree, ImportEx] = {
    case q"import ..$importersnel" =>
      ImportEx(Meta(importersnel))
  }
}

trait Imported {
  def packageName: String
}
case class NameImport(packageName: String, typeName: String) extends Imported
case class RenameImport(packageName: String, typeName: String, renamedFrom: String) extends Imported
case class WildcardImport(packageName: String) extends Imported