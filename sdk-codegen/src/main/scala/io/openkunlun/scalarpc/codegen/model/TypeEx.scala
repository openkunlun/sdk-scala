/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta._

/**
 * @author kostas.kougios
 *         Date: 02/11/17
 */
case class TypeEx private (meta: TypeEx.Meta) extends CodeEx with MetaEx.Contains {
  def name: String = meta.tpe match {
    case n: Type.Name => n.value
    case a: Type.Apply =>
      a.tpe match {
        case n: Type.Name => n.value
      }
  }

  /**
   * 返回类型的泛型
   * @return
   */
  def args: Seq[String] = meta.tpe match {
    case p: Type.Apply => p.args.map(_.syntax)
    case _             => Seq.empty[String]
  }

  override def tree: Type = meta.tpe
}

object TypeEx {

  case class Meta(tpe: Type) extends MetaEx

  def apply(name: String): TypeEx = TypeEx(Meta(Type.Name(name)))
  def apply(tpe: Type): TypeEx = TypeEx(Meta(tpe))

  trait Contains[+T] {
    def `type`: TypeEx
    def withType(t: TypeEx): T
  }

}