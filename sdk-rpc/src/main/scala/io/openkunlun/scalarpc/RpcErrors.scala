/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc

import com.caucho.hessian.io.{ Hessian2Input, Hessian2Output, SerializerFactory }

import java.io.{ ByteArrayInputStream, ByteArrayOutputStream }

trait RpcErrors {

  val factoryLocal: ThreadLocal[SerializerFactory] = ThreadLocal.withInitial(() => new SerializerFactory())

  def pack(obj: RuntimeException): Array[Byte] = {
    val out = new ByteArrayOutputStream()
    var hout: Hessian2Output = null
    try {
      hout = new Hessian2Output(out)
      hout.setSerializerFactory(factoryLocal.get())
      hout.setCloseStreamOnClose(true)
      hout.startMessage()
      hout.writeObject(obj)
      hout.completeMessage()
      hout.flush()
      out.toByteArray
    } finally {
      if (null != hout) {
        try {
          hout.close()
        } catch {
          case _: Throwable =>
        }
      }
    }
  }

  def unpack(bytes: Array[Byte]): RuntimeException = {
    var hin: Hessian2Input = null
    try {
      hin = new Hessian2Input(new ByteArrayInputStream(bytes))
      hin.setSerializerFactory(factoryLocal.get())
      hin.setCloseStreamOnClose(true)
      hin.startMessage()
      val result = hin.readObject(classOf[RuntimeException])
      hin.completeMessage()
      result match {
        case re: RuntimeException => re
        case _                    => new RuntimeException(result.asInstanceOf[Throwable])
      }
    } finally {
      if (null != hin) {
        try {
          hin.close()
        } catch {
          case _: Throwable =>
        }
      }
    }

  }
}
object RpcErrors extends RpcErrors
