/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc

import scala.annotation.ConstantAnnotation

final class RpcCall(method: String) extends ConstantAnnotation
