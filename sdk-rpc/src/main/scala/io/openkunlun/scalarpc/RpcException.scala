/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc

import io.grpc.Status

class RpcException(message: String, cause: Throwable) extends RuntimeException(message, cause) {

  def this() {
    this(null, null)
  }

  def this(message: String) {
    this(message, null)
  }

  def this(status: Status) {
    this(if (status.getDescription == null) status.getCode.toString else status.getCode + ": " + status.getDescription, status.getCause)
  }
}
